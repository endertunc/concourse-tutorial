#!/bin/sh

pwd
echo "Hello World"
#set -e
# task script is in resource-tutorial/10_job_inputs/ folder
# application input is in gopath/src/github.com/cloudfoundry-community/simple-go-web-app folder
# $GOPATH is gopath/ folder
#export GOPATH=$(pwd)/gopath:$(pwd)/gopath/src/github.com/cloudfoundry-community/simple-go-web-app/Godeps/_workspace
#cd gopath/src/github.com/cloudfoundry-community/simple-go-web-app/
#cd resource-app
export PATH=$PATH:/opt/activator/bin/
ls
#activator test
#go test ./...
